#!/bin/bash
set -e

# Run from Dockerhub image
docker run --network=stackbus-network -it --rm -p 3000:3000 --name stackbus-cli --ip 172.50.0.2  --mount type=bind,source="$(pwd)",target=/app stackbus/devconsole:1.0.0-dev.1 
