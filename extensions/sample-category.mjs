"use strict";

import { EntitySchema, Entity } from 'typeorm'

export class Category {
    
    constructor(name, description) {          
        this.name = name;
        this.description = description;
    }
}

export const CategorySchema = new EntitySchema({
    name: "Category", // Will use table name `category` as default behaviour.
    tableName: "categories", // Optional: Provide `tableName` property to override the default behaviour for table name.
    target: Category,
    columns: {
        _id: {
            primary:true,
            objectId: true,
            type: "ObjectId"
        },
        name: {
            type: "string"
        },
        description: {
            type: "string"
        }

    },
})


export default class {

    getLabel(){
        return 'Stackbus CLI Extension CATEGORY' ;
    }

    getProcessingInstructions(){
        return ["CATEGORY"];
    }

    getEntities(){
        return [CategorySchema];
    }

    processRow(row, ds){
        console.log(this.getLabel());
        console.log('Processing row: ' + row);
      
        // getting repo like this works too if name is set to "Category"
        //let repo =  ds.manager.getRepository("Category");
        let repo =  ds.manager.getRepository(Category);

        const newItem = repo.create({
            "name": row[1],
            "description":row[2]
        });
    
        return repo.save(newItem);
    }
}
