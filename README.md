# Stackbus CLI

The Stackbus CLI is a NPM CLI tool to perform actions related to the Stackbus platform.

Current version has a single "load" command which is used to load initial data into the database

## Pre-requisites
* Docker installed
* MongoDB running

## Setup 

1. Create a .env file

    ```ini
    DB_URL="mongodb://your_db_host/stackbus"
    DB_USERNAME=your_db_username_here
    DB_PASSWORD=your_db_pwd_here
    DB_SYNC=true
    DB_PORT=27017  // <= your db port
    ```

## Building 

1. Open the devconsole:
    ```bash
    ./devconsole.sh
    ```
2. Install dependencies
    ```bash
    npm install
    ```
3. Build the project
    ```bash
    npm run build
    ```

## Running command 'load'

1. Optionally add your own load extensions

    Dynamic loading extensions are 
    *.mjs files (ECMAScript modules)
    placed in any folder
    
    An Example is provided here:
    [extensions/sample-category.mjs](extensions/sample-category.mjs)

2. Prepare your Data CSV file to load from

    An Example is provided here:
    [load/sample.csv](load/sample.csv)

    Basically the first column is always the KEY that is used to determine which load handler will process the row. 
    ```csv
    PLATFORM|TalenIT|TAlenIT Platform
    CATEGORY|Bags|Bags category
    ```
    In our example:

     "PLATFORM" is handled by the built-in processor: [src/buildin/platform.extension.ts](src/buildin/platform.extension.ts)

     "CATEGORY" is handled by the extension processor: [extensions/sample-category.mjs](extensions/sample-category.mjs)

3. Open the devconsole:

    ```bash
    ./devconsole.sh
    ```

4. Run the (previously built) CLI program

    ```bash
    node dist/stackbus.js load /app/load/sample.csv -x /app/extensions
    ```
    * load - command being run
    * /app/load/sample.csv - file to load data from
    * -x /app/extensions - (optional) folder containing *.mjs files to load as extensions
    * NOTE: the reason we use path "/app" is because that is the location that is mapped to the root of your project when inside the devconsole 

## Installing the built CLI globally

1. Enter the devconsole
   ```bash
    ./devconsole.sh
    ```
2. Build
   ```bash
    npm install
    npm run build
    ```
3. Install globally
   ```bash
    cd dist
    npm install -g .
    cd ..
    ```
4. Run load using the installed binary (still from devconsole)
   ```bash
    stackbus load /app/load/sample.csv -x /app/extensions
    ```

5. Optionally delete the installed binary

    You will have to do this before you try to install globally again. From devconsole do

   ```bash
    npm uninstall -g @stackbus/stackbus-cli
    ```
