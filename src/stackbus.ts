#!/usr/bin/env node
import { Command } from 'commander';
import { StackbusCliDatasource } from './datasource'
import { System } from '@stackbus/community-core-models';
import { StackbusExtensions, IStackbusCliExtension } from './extensions';
import * as fs from 'fs';
import Papa from 'papaparse';
import { EntitySchema, MixedList, DataSource} from "typeorm";
import { PlatformExtension } from './builtin/platform.extension';
import utils from "./utils";

//https://blog.logrocket.com/building-typescript-cli-node-js-commander/

const program = new Command();

async function load(filename:any, options:any, command:any) { 

  //console.log('command: '+ command.name());
  //console.log('filename: '+ filename);
  //console.log('options: '+ JSON.stringify(options,null,2));
  
  if ((null == filename)||(filename == undefined)){
    utils.abort("Invalid or missing filemane");
  }

  if (!fs.existsSync(filename)){
    utils.abort('File does not exist: ' + filename);    
  }
  if (!fs.lstatSync(filename).isFile())
  {
    utils.abort('Path is not a file: ' + filename);    
  }

  const csvFile = fs.readFileSync(filename, 'utf8');

  let extensions:StackbusExtensions = new StackbusExtensions(options.extensions);

  loadBuiltInExtensions(extensions);
  
  await extensions.loadExtensions();

  //let entities:MixedList<Function | string | EntitySchema> = [Platform, System];
  let entities:MixedList<Function | string | EntitySchema> = [System];

  let e:(Function | string | EntitySchema)[] = extensions.getEntities();
  entities.splice(-1,0, ...e);

  console.log('LOADED ENTITIES: ');
  entities.map( (item:any)=>{
    console.log( '    Entity: ' + item.toString());
  });
  console.log('Done loading extensions');

  console.log('ENTITIES: ' + entities);

  let ds: StackbusCliDatasource = new StackbusCliDatasource();
  ds.init(entities);
  await ds.connect();

  let theDataSource:DataSource = ds.getDatasource();

  if (theDataSource == undefined){
    utils.abort('UNDEFINED DS');
  }

  if (theDataSource.manager == undefined){
    utils.abort('UNDEFINED MANAGER');
  }

  let promises:Promise<any>[] = [];

  let fn = function(row:any) {

    let fnds = theDataSource;
    let arr:string[] = (row.data) as string[];

    let xt:IStackbusCliExtension|undefined = extensions.getExtensionFor(arr[0]);
    if (xt == undefined){
      console.error('NO HANDLER FOUND FOR ROW: ' + arr);
    }else{
      promises.push( xt.processRow(arr, fnds) );
    }
  };
  

  Papa.parse(csvFile, {
    step: fn,
    complete: function() {

      let all:Promise<any> = Promise.all(promises);
      all.then(
        ()=>{
          console.log('CALLED THEN from All Promises');
        }
      ).catch( (err) =>{
        console.error('CALLED CATCH from All Promises: ' + err);
      }).finally(()=>{
        console.log('CALLED FINALLY from All Promises');
        console.log("DONE PROCESSING CSV FILE: " + filename);
        console.log('CLOSING DB CONNECTION')
        ds.closeConnection();
      });
      
    }
  });
}



async function main() {

    program
    .command('load')
    .argument('<filename>')
    .option('-x, --extensions <extdir>', 'Extensions directory')
    .action(load);

  await program.parseAsync(process.argv);

}

try {
  main();
}
catch (error) {
  console.error(error);
}
 
// Support functions below ==================

function loadBuiltInExtensions(extensions: StackbusExtensions) {
  let newExt: IStackbusCliExtension = new PlatformExtension();
  let instructions: string[] = newExt.getProcessingInstructions();
  for (let i = 0; i < instructions.length; i++) {
    extensions.addExtension(instructions[i], newExt);
  }
}












    