import { EntitySchema, MixedList } from "typeorm";
import utils from "./utils";
import { glob } from 'glob'
import { join, sep } from 'path'

export interface IStackbusCliExtension{
    getLabel():string;
    getProcessingInstructions():string[];
    getEntities(): (Function | string | EntitySchema)[];
    processRow(row:any, ds:any):any;
}

export class StackbusExtensions{

    files:string[];
    folders:string[];
    extensionsMap = new Map<string, IStackbusCliExtension>();

    constructor(loc:string){
        this.files = [];
        this.folders = [];
        let arr:string[] = loc.split(',');

        arr.forEach( str=>{ 
            if (utils.isDirectory(str)){
                if (str.endsWith('/')){
                    str = str.substring(0,str.length-1);
                }else if (str.endsWith(sep)){
                    str = str.substring(0,str.length-1);
                }
                this.folders.push(str);
            }else if (utils.isFile(str)){
                this.files.push(str);
            }else{
                if (null != str){
                    if (str.trim() == ""){
                        console.log('Ignoring empty string in list of extensions directory: ' + loc);
                    }else{
                        utils.abort('Ignoring Invalid path for file or directory: ' + str);
                    }
                }
            }  
        });
    }

    addExtension(key:string, extension: IStackbusCliExtension){
        this.extensionsMap.set(key, extension);
    }

    async loadExtensions(){
        for (let i = 0;i < this.folders.length;i++){
            const d = this.folders[i] + '/*.mjs'; 
            const jsfiles = await glob(d, { ignore: 'node_modules/**' });
            for(let j = 0;j < jsfiles.length;j++){
                
                try{
                    let c = await import(jsfiles[j]);
                    let ext:IStackbusCliExtension = new c.default();
                    console.log('Loaded extension: ' + jsfiles[j] );
                    let instructions:string[] = ext.getProcessingInstructions();
                    
                    for(let k = 0;k < instructions.length;k++){
                        this.extensionsMap.set(instructions[k], ext);

                        console.log('    Assigned handling of: ' + instructions[k] + ' to extension: ' + jsfiles[j] + ' : ' + utils.nn( ext.getLabel() ));
                    }
                }catch(error){
                    utils.abort('Error loading extension: ' + jsfiles[j] + ' : ' + error);
                }
            }
        }

        for (let i = 0;i < this.files.length;i++){
            try{
                let ext:IStackbusCliExtension = await import(this.files[i]);
                let instructions:string[] = ext.getProcessingInstructions();
                
                for(let k = 0;k < instructions.length;k++){
                    this.extensionsMap.set(instructions[k], ext);
                }
                
            }catch(error){
                utils.abort('Error loading extension: ' + this.files[i] + ' : ' + error);
            }
        }
    }

    getExtensionFor(key:string):IStackbusCliExtension|undefined {
        return this.extensionsMap.get(key);
    }

    getEntities(): (Function | string | EntitySchema)[]{
        
        let retValue:(Function | string | EntitySchema)[] = [];

        for (let value of this.extensionsMap.values()){
            const i:IStackbusCliExtension = value as IStackbusCliExtension;
            let arr:(Function | string | EntitySchema)[] = i.getEntities();
            
            if (arr.length > 0){
                retValue.splice(-1,0,...arr);
            }
        }
        return retValue;
    }

}
