
import * as fs from 'fs';

class Utils {

    abort(message: string): never {
        console.error(message);
        process.exit(1);
    }
    nn(str:any):string{
        if (str == undefined){
            return "undefined";
        }
        if (str == null){
            return "null";
        }
        if (typeof str == 'string')
            return str;

        return str.toString();
    }

    isFile(filename:string):boolean{
        if ((null == filename)||(filename == undefined)){
            return false;
        }
        
        if (!fs.existsSync(filename)){
            return false;
        }
        return fs.lstatSync(filename).isFile();
    }

    isDirectory(dirname:string):boolean{
        if ((null == dirname)||(dirname == undefined)){
            return false;
        }
        
        if (!fs.existsSync(dirname)){
            return false;
        }
        return fs.lstatSync(dirname).isDirectory();
    }
}

export default new Utils();