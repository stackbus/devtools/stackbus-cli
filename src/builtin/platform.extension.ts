import { DataSource, EntitySchema, Repository } from "typeorm"
import { Platform } from "@stackbus/community-core-models"
import { IStackbusCliExtension } from "src/extensions";


export class PlatformExtension implements IStackbusCliExtension{
    getLabel(): string {
        return 'PLATFORM processor';
    }
    getProcessingInstructions(): string[] {
        return ['PLATFORM'];
    }
    getEntities(): (string | Function | EntitySchema<any>)[] {
        return [Platform];
    }
    processRow(row: any, ds: any) {
        console.log(this.getLabel());
        console.log('Processing row: ' + row);
      
        let repo =  ds.manager.getRepository(Platform);

        const newItem = repo.create({
            "_id": row[1],
            "label":row[2]
        });
    
        return repo.save(newItem);
    }
}