import { DataSource, EntitySchema, MixedList } from "typeorm";
import 'dotenv/config';

export class StackbusCliDatasource{

    datasource:DataSource;

    constructor(){
    }

    init(entities: MixedList<Function | string | EntitySchema>){
      let port:number = 27017;

      if (process.env.DB_PORT != undefined){
        if (!isNaN(+process.env.DB_PORT)){
          port = Number(process.env.DB_PORT).valueOf();
        }
      }
      
      let sync:boolean = false;
      if (process.env.DB_SYNC != undefined){
        if (process.env.DB_SYNC == "true"){
          sync = true;
        }
      }
      
      this.datasource = new DataSource({
          type: "mongodb",
          url: process.env.DB_URL,
          port: port,
          username: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          synchronize: sync,
          entities: entities
      });
    }

    async connect(){
        return this.datasource.initialize();
        
    }

    async closeConnection(){
        this.datasource.destroy();
    }

    getDatasource():DataSource{
      return this.datasource;
    }

}